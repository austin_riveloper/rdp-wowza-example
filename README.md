Installation instructions for setting up Wowza and configuring Kurento for passthrough content.

Using a terminal run the following GCloud commands to configure the servers firewall to accept connections for Stun, Turn, and Wowza protocols.

`gcloud compute firewall-rules create kurento --allow tcp:8443`

`gcloud compute firewall-rules create wowza --allow tcp:8084-8089`

`gcloud compute firewall-rules create udpports --allow udp:3000-20000`

`gcloud compute firewall-rules create webserver --allow tcp:80`

`gcloud compute firewall-rules create wowza-viewing-port --allow tcp:1935,icmp`


On the server run the following commands need to be run to configure a Google Cloud instance to run Wowza, and Kurento successfully.

`echo "deb http://ubuntu.kurento.org trusty kms6" | sudo tee /etc/apt/sources.list.d/kurento.list`

`wget -O - http://ubuntu.kurento.org/kurento.gpg.key | sudo apt-key add -`

`sudo apt-get update`

`sudo apt-get upgrade`

`sudo apt-get install kurento-media-server-6.0`

`sudo wget https://www.wowza.com/downloads/WowzaStreamingEngine-4-6-0/WowzaStreamingEngine-4.6.0-linux-x64-installer.run`

`sudo chmod +x WowzaStreamingEngine-4.6.0-linux-x64-installer.run`

`sudo ./WowzaStreamingEngine-4.6.0-linux-x64-installer.run`

Follow the onscreen directions to install Wowza Media Server. 

Upon installing the Wowza Media Server upload the template SDP file under the following directory:

`/usr/local/WowzaStreamingEngine/content/template.sdp` 

You will need to configure a webserver on the cloud instance using the files located under the webserver directory.


Once everything is setup you should be able to run the server to view the demo.

`nodejs server.js`