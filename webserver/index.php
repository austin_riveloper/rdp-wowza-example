<?php
//phpinfo();

function createSDPFile($name){
	$randomPort = rand(40000, 50000);
	$randomPort2 = rand(40000, 50000);
	//Webserver needs permissions to place files in the following directory.
	$templateSDP = file_get_contents("/usr/local/WowzaStreamingEngine/content/template.sdp");
	$templateSDP = str_replace("RANDOMPORT", strval($randomPort), $templateSDP);
	$templateSDP = str_replace("RANDOMPORT2", strval($randomPort2), $templateSDP);
	file_put_contents("/usr/local/WowzaStreamingEngine/content/".$name.".sdp", $templateSDP);
	return $templateSDP;
}

function connectToStreamFile($file, $appName){
	$file = str_replace("\.sdp", "", $file);

	//Replace admin:admin with Wowza Login.
	return json_decode(exec('curl --digest -u "admin:admin" -X PUT --header \'Accept:application/json; charset=utf-8\' --header \'Content-type:application/json; charset=utf-8\' "http://localhost:8087/v2/servers/_defaultServer_/vhosts/_defaultVHost_/sdpfiles/'.$file.'/actions/connect?connectAppName='.$appName.'&appInstance=_definst_&mediaCasterType=rtp"'), true);
}


//generate sample
//You would generate a file for a users uid.
$sdp = createSDPFile('hello_world'); //generate uids and connect users to the sdp
connectToStreamFile('hello_world.sdp', 'live'); //normally the app is called live
echo json_encode(array("sdp" => $sdp));
?>